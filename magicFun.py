class circle:
    p=3.14

    def __init__(self,r):
        print('Values Created')
        self.r=r

    def __str__(self):                      # magic function; str stands for string representation
        print('Called')
        print(self.p*self.r*self.r)

    def __del__(self):                      # destructor; never call it
        print('del called before',self.r)
        del self.r
        print('Values Destroyed')
        print(self.r)

o=circle(10)
o.__str__()