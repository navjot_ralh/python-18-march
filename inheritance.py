class details:
    type='Student'
    def __init__(self):
        print('Parent Class')

    def info(self,name,roll_no):
        print('Name:{} Roll No:{}'.format(name,roll_no))

class fee(details):                 # inheritance; 3 types: single, multiple, multi-level
    print('Child Class')
    def total(self,tution,academic):
        print(tution+academic)

obj=fee()
obj.total(1000,2000)
obj.info('Navi',1)
print(obj.type)

pobj=details()
pobj.info('Peter',2)
pobj.total(10,10)               # will produce error; bcz parent class can't access members of child class