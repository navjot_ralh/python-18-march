class car:
    # def set_val(self):
    def __init__(self,color,brand):                 # constructor
        self.c=color
        self.b=brand

    def print_details(self):
        print('Color:{} Brand:{}'.format(self.c,self.b))


obj=car('White','BMW')
# print(obj)        # returns memory location
obj.print_details()

obj1=car('Black','Toyota')
obj1.print_details()

cl=input('Enter Car Color: ')
br=input('Enter Car Brand: ')
obj2=car(cl,br)
obj2.print_details()