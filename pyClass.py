class person:
    category='Human'
    def set_name(self,first,last):
        # return first+last
        self.f=first
        self.l=last

    def print_name(self):
        return 'First Name:{} Last Name:{} Category:{}'.format(self.f,self.l,self.category)    # to access parameters of one function in another

p1=person()
# print(type(p1))                     # returns <class '__main__.person'> which means it is an object of person class
# print(p1.category)
# print(p1.__class__.category)        # same as above

# print(p1.get_name('Peter','Parker'))
# print(p1.print_name())

p1.set_name('Navjot','Ralh')
print(p1.print_name())

p2=person()
p2.set_name('Peter','Parker')
print(p2.print_name())